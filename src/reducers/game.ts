import produce from 'immer';
import { Capital } from 'api';
import { Action } from 'actions';
import { Level } from 'utils/levels';
import {
  CAPITALS_LOADED,
  CHOOSE_CITY,
  SUBMIT_ANSWER,
  SUBMIT_ANSWER_FAILED,
  WEATHER_DATA_LOADED,
} from 'constants/actionTypes';

export interface Game {
  capitals: Capital[];
  levels: Level[];
}

export default function gameReducer(state: Game, action: Action): Game {
  if (state === undefined) {
    return { capitals: [], levels: [] };
  }

  if (action.type === CAPITALS_LOADED) {
    return { ...state, ...action.payload };
  }

  if (action.type === CHOOSE_CITY) {
    return produce(state, draft => {
      draft.levels[action.payload.levelIndex].choice = action.payload.cityNumber;
    });
  }

  if (action.type === SUBMIT_ANSWER) {
    return produce(state, draft => {
      draft.levels[action.payload.levelIndex].submitted = true;
    });
  }

  if (action.type === SUBMIT_ANSWER_FAILED) {
    return produce(state, draft => {
      draft.levels[action.payload.levelIndex].submitted = false;
    });
  }

  if (action.type === WEATHER_DATA_LOADED) {
    return produce(state, draft => {
      const { city1Temp, city2Temp } = action.payload;

      if (city1Temp > city2Temp) {
        draft.levels[action.payload.levelIndex].actual = 1;
      } else if (city1Temp < city2Temp) {
        draft.levels[action.payload.levelIndex].actual = 2;
      } else {
        draft.levels[action.payload.levelIndex].actual = draft.levels[action.payload.levelIndex].choice;
      }

      draft.levels[action.payload.levelIndex].city1Temp = city1Temp;
      draft.levels[action.payload.levelIndex].city2Temp = city2Temp;
    });
  }

  return state;
}
