import { State } from 'store';

export function selectError(state: State) {
  return state.error;
}
