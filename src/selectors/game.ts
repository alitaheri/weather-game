import { State } from 'store';
import { createSelector } from 'reselect';

export function selectCapitals(state: State) {
  return state.game.capitals;
}

export function selectLevels(state: State) {
  return state.game.levels;
}

export function selectLevel(state: State, level: number) {
  return state.game.levels[level];
}

export const selectHasHistory = createSelector(
  selectLevels,
  levels => levels.some(level => level.choice !== null),
);

export const selectScore = createSelector(
  selectLevels,
  levels => levels.reduce((count, level) => {
    if (level.submitted && level.choice !== null && level.actual !== null && level.choice === level.actual) {
      return count + 1;
    }

    return count;
  }, 0),
);
