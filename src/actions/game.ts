import { Capital } from 'api';
import { Level } from 'utils/levels';
import {
  START_GAME,
  RESTART_GAME,
  CAPITALS_LOADED,
  CHOOSE_CITY,
  SUBMIT_ANSWER,
  SUBMIT_ANSWER_FAILED,
  WEATHER_DATA_LOADED,
} from 'constants/actionTypes';

export function startGame() {
  return {
    type: START_GAME,
  };
}

export function restartGame() {
  return {
    type: RESTART_GAME,
  };
}

export function capitalsLoaded(capitals: Capital[], levels: Level[]) {
  return {
    type: CAPITALS_LOADED,
    payload: { capitals, levels },
  };
}

export function chooseCity(levelIndex: number, cityNumber: 1 | 2) {
  return {
    type: CHOOSE_CITY,
    payload: { levelIndex, cityNumber },
  };
}

export function submit(levelIndex: number, level: Level) {
  return {
    type: SUBMIT_ANSWER,
    payload: { levelIndex, level },
  };
}

export function submitFailed(levelIndex: number) {
  return {
    type: SUBMIT_ANSWER_FAILED,
    payload: { levelIndex },
  };
}

export function weatherDataLoaded(levelIndex: number, city1Temp: number, city2Temp: number) {
  return {
    type: WEATHER_DATA_LOADED,
    payload: { levelIndex, city1Temp, city2Temp },
  };
}

export type GameAction =
  | ActionOf<typeof startGame>
  | ActionOf<typeof restartGame>
  | ActionOf<typeof capitalsLoaded>
  | ActionOf<typeof chooseCity>
  | ActionOf<typeof submit>
  | ActionOf<typeof submitFailed>
  | ActionOf<typeof weatherDataLoaded>;
