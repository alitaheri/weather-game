import { combineReducers } from 'redux';
import gameReducer from './game';
import settingsReducer from './settings';
import errorReducer from './error';

export default combineReducers({
  game: gameReducer,
  settings: settingsReducer,
  error: errorReducer,
});
