import { withStyles as muiWithStyles, StyleRules, Theme } from '@material-ui/core/styles';

export { createStyles as css } from '@material-ui/core/styles';

export type SheetWrapper<K extends string> = <C>(component: C) => C;

export type Styled<S> = S extends SheetWrapper<infer K> ? { classes: Record<K, string> } : never;

export function createStyles<K extends string>(styles: StyleRules<K> | ((theme: Theme) => StyleRules<K>)): SheetWrapper<K> {
  return muiWithStyles(styles, { withTheme: true }) as any;
}
