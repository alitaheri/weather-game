import React, { Component, ComponentClass } from 'react';
import { Switch, Redirect, Route } from 'react-router';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import { createStyles, css, Styled } from 'utils/style';
import { State } from 'store';
import { selectCapitals, selectHasHistory, selectError } from 'selectors';
import { hideError, restartGame } from 'actions';
import SpeedDial from 'components/SpeedDial';
import ErrorReporter from 'components/ErrorReporter';
import Initial from 'routes/Initial';
import Finish from 'routes/Finish';
import Settings from 'routes/Settings';
import History from 'routes/History';
import Game from 'routes/Game';

const styles = createStyles(theme => css({
  '@global': {
    'html': {
      display: 'flex',
      width: '100%',
      height: '100%',
    },
    'body, #root': {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
  },
  main: {
    display: 'flex',
    position: 'relative',
    flex: 1,
    margin: 'auto',
    width: 800,
    maxHeight: 600,
  },
}));

function mapStateToProps(state: State) {
  return {
    capitals: selectCapitals(state),
    hasHistory: selectHasHistory(state),
    error: selectError(state),
  }
}

const actions = { hideError, push, restartGame };

type Props = Styled<typeof styles> & ReturnType<typeof mapStateToProps> & Actions<typeof actions>;

@styles
@connect(mapStateToProps, actions, null, { pure: false })
class App extends Component<Props> {
  private handleHistoryClick = () => {
    const { push } = this.props;

    push('/history');
  }

  private handleSettingsClick = () => {
    const { push } = this.props;

    push('/settings');
  }

  private handleRestartClick = () => {
    const { restartGame } = this.props;

    restartGame();
  }

  public render() {
    const { classes, capitals, hasHistory, hideError, error } = this.props;

    return (
      <Paper square={true} className={classes.main}>
        <Switch>
          <Route path="/" exact={true} component={Initial} />
          <Route path="/finish" exact={true} component={Finish} />
          <Route path="/settings" exact={true} component={Settings} />
          <Route path="/history" exact={true} component={History} />
          <Route path="/level/:level" exact={true} component={Game} />
          {capitals.length === 0 && <Redirect to="/" />}
        </Switch>
        <Route>
          {({ location }) => location.pathname !== '/settings' && location.pathname !== '/history' && (
            <SpeedDial
              hideHistory={!hasHistory}
              hideRestart={!hasHistory}
              onHistoryClick={this.handleHistoryClick}
              onSettingsClick={this.handleSettingsClick}
              onRestartClick={this.handleRestartClick} />
          )}
        </Route>
        <ErrorReporter onClose={hideError} error={error} />
      </Paper>
    );
  }
}

export default App as ComponentClass<{}>;
