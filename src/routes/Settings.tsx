import React, { Component, ComponentClass } from 'react';
import { createStyles, css, Styled } from 'utils/style';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import Mousetrap from 'mousetrap';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Close from '@material-ui/icons/Close';
import { switchTemperatureUnit } from 'actions';
import { State } from 'store';
import { selectTemperatureUnit } from 'selectors';

const styles = createStyles(theme => css({
  root: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    flex: 1,
    padding: theme.spacing.unit * 2,
  },
  goBackButton: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
}));

function mapStateToProps(state: State) {
  return {
    units: selectTemperatureUnit(state),
  }
}

const actions = { switchTemperatureUnit, goBack };

type Props = Styled<typeof styles> & Actions<typeof actions> & ReturnType<typeof mapStateToProps>;

@styles
@connect(mapStateToProps, actions)
class Settings extends Component<Props> {

  private handleUnitChange = (event: any, unit: Props['units']) => {
    const { switchTemperatureUnit } = this.props;

    switchTemperatureUnit(unit);
  }

  private handleGoBack = () => {
    const { goBack } = this.props;

    goBack();
  }

  public componentDidMount() {
    Mousetrap.bind('esc', this.handleGoBack);
  }

  public componentWillUnmount() {
    Mousetrap.unbind('esc');
  }

  public render() {
    const { classes, units } = this.props;
    return (
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom={true}>
          Pick A Temperature Unit
        </Typography>
        <RadioGroup value={units} onChange={this.handleUnitChange}>
          <FormControlLabel value="celsius" control={<Radio />} label="Celsius" />
          <FormControlLabel value="fahrenheit" control={<Radio />} label="Fahrenheit" />
        </RadioGroup>
        <Button className={classes.goBackButton} variant="fab" onClick={this.handleGoBack}>
          <Close />
        </Button>
      </div>
    );
  }
}

export default Settings as ComponentClass<{}>;
