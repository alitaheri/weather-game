import { SHOW_ERROR, HIDE_ERROR } from 'constants/actionTypes';
import { Action } from 'actions';

export default function errorReducer(state: string | null, action: Action): string | null {
  if (state === undefined) {
    return null;
  }

  if (action.type === SHOW_ERROR) {
    return action.payload;
  }

  if (action.type === HIDE_ERROR) {
    return null;
  }

  return state;
}
