import { takeLatest, call, put } from 'redux-saga/effects';
import { SUBMIT_ANSWER } from 'constants/actionTypes';
import { ExtractActionByType, weatherDataLoaded, showError, submitFailed } from 'actions';
import { getTemperature } from 'api';

function* submitAnswerWorker({ payload }: ExtractActionByType<typeof SUBMIT_ANSWER>) {
  try {
    const city1Temp = yield call(getTemperature, payload.level.city1.latlng);
    const city2Temp = yield call(getTemperature, payload.level.city2.latlng);

    yield put(weatherDataLoaded(payload.levelIndex, city1Temp, city2Temp));
  } catch {
    yield put(submitFailed(payload.levelIndex));

    yield put(showError('Failed to Check Your Answer, Please Try Again'));
  }
}

export default function* submitAnswerWatcher() {
  yield takeLatest(SUBMIT_ANSWER, submitAnswerWorker);
}
