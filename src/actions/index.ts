import { GameAction } from './game';
import { ErrorAction } from './error';
import { SettingAction } from './setting';

export * from './game';
export * from './error';
export * from './setting';

export type Action =
  | ErrorAction
  | GameAction
  | SettingAction;

export type ExtractActionByType<T extends Action['type'], A extends Action = Action> = A extends { type: T } ? A : never;
export type PayloadOfAction<T extends Action['type']> = { [P in Action['type']]: ExtractActionByType<P> }[T]['payload'];
