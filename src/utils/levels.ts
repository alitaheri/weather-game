import sample from 'lodash/sample';
import { Capital } from 'api';

export interface Level {
  city1: Capital;
  city2: Capital;
  city1Temp: null | number;
  city2Temp: null | number;
  submitted: boolean;
  choice: null | 1 | 2;
  actual: null | 1 | 2;
}

export function buildLevel(city1: Capital, city2: Capital): Level {
  return {
    city1,
    city2,
    city1Temp: null,
    city2Temp: null,
    submitted: false,
    choice: null,
    actual: null,
  };
}

export function validateSamples(levels: Level[], sample1: Capital, sample2: Capital) {
  if (sample1 === sample2) {
    return false;
  }

  for (const { city1, city2 } of levels) {
    if (city1 === sample1 && city2 === sample2) {
      return false;
    }

    if (city1 === sample2 && city2 === sample1) {
      return false;
    }
  }

  return true;
}

// Builds unique levels
export function buildLevels(capitals: Capital[], levels: number) {
  const result: Level[] = [];

  for (let i = 0; i < levels; i++) {
    let sample1: Capital;
    let sample2: Capital;

    do {
      sample1 = sample(capitals)!;
      sample2 = sample(capitals)!;
    } while (!validateSamples(result, sample1, sample2));

    result.push(buildLevel(sample1, sample2));
  }

  return result;
}
