import React, { Component, ComponentClass } from 'react';
import cx from 'classnames';
import { createStyles, css, Styled } from 'utils/style';
import { getTemperatureString } from 'utils/temperature';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import Mousetrap from 'mousetrap';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SvgIcon from '@material-ui/core/SvgIcon';
import Close from '@material-ui/icons/Close';
import Check from '@material-ui/icons/Check';
import { State } from 'store';
import { selectTemperatureUnit, selectLevels } from 'selectors';

const styles = createStyles(theme => css({
  root: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    flex: 1,
    padding: theme.spacing.unit * 2,
  },
  goBackButton: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
  trableWrapper: {
    maxHeight: 450,
    overflowY: 'auto',
  },
  scored: {
    color: '#00e676',
  },
  failed: {
    color: '#ff3d00',
  },
}));

function mapStateToProps(state: State) {
  return {
    units: selectTemperatureUnit(state),
    levels: selectLevels(state),
  }
}

const actions = { goBack };

type Props = Styled<typeof styles> & Actions<typeof actions> & ReturnType<typeof mapStateToProps>;

@styles
@connect(mapStateToProps, actions)
class History extends Component<Props> {

  private handleGoBack = () => {
    const { goBack } = this.props;

    goBack();
  }

  public componentDidMount() {
    Mousetrap.bind('esc', this.handleGoBack);
  }

  public componentWillUnmount() {
    Mousetrap.unbind('esc');
  }

  public render() {
    const { classes, units, levels } = this.props;

    return (
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom={true}>
          Game History
        </Typography>
        <div className={classes.trableWrapper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Level</TableCell>
                <TableCell>City 1</TableCell>
                <TableCell>City 1 Temp</TableCell>
                <TableCell>City 2</TableCell>
                <TableCell>City 2 Temp</TableCell>
                <TableCell>Scored</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {levels.map((level, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {index + 1}
                    </TableCell>
                    <TableCell>{level.city1.country}, {level.city1.name}</TableCell>
                    <TableCell>{level.city1Temp !== null ? getTemperatureString(level.city1Temp, units) : ''}</TableCell>
                    <TableCell>{level.city2.country}, {level.city2.name}</TableCell>
                    <TableCell>{level.city2Temp !== null ? getTemperatureString(level.city2Temp, units) : ''}</TableCell>
                    <TableCell>
                      {level.actual === null ? '' : (
                        <SvgIcon
                          className={cx({
                            [classes.scored]: level.actual === level.choice,
                            [classes.failed]: level.actual !== level.choice,
                          })}
                        >
                          {level.actual === level.choice ? <Check /> : <Close />}
                        </SvgIcon>
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
        <Button className={classes.goBackButton} variant="fab" onClick={this.handleGoBack}>
          <Close />
        </Button>
      </div>
    );
  }
}

export default History as ComponentClass<{}>;
