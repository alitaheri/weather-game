import axios from 'axios';

const LIST_OF_ALL_COUNTRIES = 'https://restcountries.eu/rest/v2/all';
const GET_WEATHER = 'https://api.openweathermap.org/data/2.5/weather';

const OPEN_WEATHER_MAP_API_KEY = 'efdafa43748bc43af5890bcc5f8102a7';

export interface Capital {
  name: string;
  country: string;
  latlng: [number, number];
}

export async function getAllCapitals(): Promise<Capital[]> {
  const response = await axios.get(LIST_OF_ALL_COUNTRIES);

  return response.data.map((item: any) => ({
    name: item.capital,
    country: item.name,
    latlng: item.latlng,
  }));
}

export async function getTemperature(latlng: [number, number]): Promise<number> {
  const response = await axios.get(GET_WEATHER, {
    params: {
      appid: OPEN_WEATHER_MAP_API_KEY,
      units: 'metric',
      lat: latlng[0],
      lon: latlng[1],
    }
  });

  return Math.round(response.data.main.temp);
}
