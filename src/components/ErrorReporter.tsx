import React, { Component, ComponentClass } from 'react';
import { createStyles, css, Styled } from 'utils/style';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const styles = createStyles(theme => css({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
}));

export interface ErrorReporterProps {
  error: string | null;
  onClose(): void;
}

type Props = ErrorReporterProps & Styled<typeof styles>;

@styles
class ErrorReporter extends Component<Props> {
  public render() {
    const { classes, error, onClose } = this.props;
    return (
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={!!error}
        autoHideDuration={4000}
        onClose={onClose}
        message={<span>{error}</span>}
        action={[(
          <IconButton
            key="close"
            color="inherit"
            className={classes.close}
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        )]} />
    );
  }
}

export default ErrorReporter as ComponentClass<ErrorReporterProps>;
