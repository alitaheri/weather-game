import { fork } from 'redux-saga/effects';
import loadCapitals from './loadCapitals';
import submitAnswer from './submitAnswer';

export default function* rootSaga() {
  yield fork(loadCapitals);
  yield fork(submitAnswer);
}
