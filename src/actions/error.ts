import { SHOW_ERROR, HIDE_ERROR } from 'constants/actionTypes';

export function showError(error: string) {
  return {
    type: SHOW_ERROR,
    error: true,
    payload: error,
  };
}

export function hideError() {
  return {
    type: HIDE_ERROR,
  };
}

export type ErrorAction =
  | ActionOf<typeof showError>
  | ActionOf<typeof hideError>;
