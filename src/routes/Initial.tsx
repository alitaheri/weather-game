import React, { Component, ComponentClass } from 'react';
import { createStyles, css, Styled } from 'utils/style';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { startGame } from 'actions';

const styles = createStyles(theme => css({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
    margin: 'auto',
  },
  button: {
    width: 300,
  }
}));

const actions = { startGame };

type Props = Styled<typeof styles> & Actions<typeof actions>;

@styles
@connect(null, actions)
class Initial extends Component<Props> {
  private handleStartButtonClick = () => {
    const { startGame } = this.props;

    startGame();
  }

  public render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Typography variant="display2" gutterBottom={true} align="center">
          Guess The Weather
        </Typography>
        <Button
          classes={{ root: classes.button }}
          variant="raised"
          color="secondary"
          onClick={this.handleStartButtonClick}
        >
          Start The Game
        </Button>
      </div>
    );
  }
}

export default Initial as ComponentClass<{}>;
