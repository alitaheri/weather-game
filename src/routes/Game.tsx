import React, { Component, ComponentClass } from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import { replace } from 'connected-react-router';
import { createStyles, css, Styled } from 'utils/style';
import { getTemperatureString, getWarmth } from 'utils/temperature';
import { LEVEL_COUNT } from 'constants/config';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { chooseCity, submit } from 'actions';
import { State } from 'store';
import { selectLevel, selectScore, selectTemperatureUnit } from 'selectors';
import CityButton from 'components/CityButton';

const styles = createStyles(theme => css({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
  },
  cityButtons: {
    display: 'flex',
  },
  cityButton: {
    margin: theme.spacing.unit * 2,
  },
  buttons: {
    width: 400,
    display: 'flex',
  },
  submit: {
    margin: theme.spacing.unit,
    flex: 3,
  },
  next: {
    margin: theme.spacing.unit,
    flex: 1,
  },
}));

export type GameProps = RouteComponentProps<{ level: string }>;

function mapStateToProps(state: State, ownProps: GameProps) {
  return {
    level: selectLevel(state, Number(ownProps.match.params.level) - 1),
    score: selectScore(state),
    units: selectTemperatureUnit(state),
  }
}

const actions = { chooseCity, submit, replace };

type Props = GameProps & Styled<typeof styles> & Actions<typeof actions> & ReturnType<typeof mapStateToProps>;

@styles
@connect(mapStateToProps, actions)
class Game extends Component<Props> {
  private createHandleCityChoose = (cityNumber: 1 | 2) => () => {
    const { match: { params: { level: levelString } }, chooseCity } = this.props;
    const levelIndex = Number(levelString) - 1;

    chooseCity(levelIndex, cityNumber);
  }

  private handleSubmit = () => {
    const { match: { params: { level: levelString } }, submit, level } = this.props;
    const levelIndex = Number(levelString) - 1;

    submit(levelIndex, level);
  }

  private handleNext = () => {
    const { match: { params: { level: levelString } }, replace } = this.props;
    const nextLevel = Number(levelString) + 1;
    const last = Number(levelString) === LEVEL_COUNT;

    if (last) {
      replace(`/finish`);
    } else {
      replace(`/level/${nextLevel}`);
    }
  }

  public render() {
    const { classes, match: { params: { level: levelString } }, level, score, units } = this.props;

    if (!level) {
      return <Redirect to="/" />;
    }

    const accepted = level.actual !== null && level.actual === level.choice;
    const failed = level.actual !== null && level.actual !== level.choice;

    const last = Number(levelString) === LEVEL_COUNT;

    return (
      <div className={classes.root}>
        <Typography variant="display2" align="center">
          Level {levelString} of {LEVEL_COUNT} (Score: {score})
        </Typography>
        <Typography variant="caption" gutterBottom={true} align="center">
          Which City is Warmer?
        </Typography>
        <div className={classes.cityButtons}>
          <CityButton
            key={`${levelString}-1`} // Needed to reload the map across levels
            onClick={this.createHandleCityChoose(1)}
            className={classes.cityButton}
            selected={level.choice === 1}
            disabled={level.submitted}
            warmth={getWarmth(level, 1)}
            temperature={level.city1Temp !== null ? getTemperatureString(level.city1Temp, units) : null}
            accepted={accepted}
            failed={failed}
            city={level.city1} />
          <CityButton
            key={`${levelString}-2`} // Needed to reload the map across levels
            onClick={this.createHandleCityChoose(2)}
            className={classes.cityButton}
            selected={level.choice === 2}
            disabled={level.submitted}
            warmth={getWarmth(level, 2)}
            accepted={accepted}
            failed={failed}
            temperature={level.city2Temp !== null ? getTemperatureString(level.city2Temp, units) : null}
            city={level.city2} />
        </div>
        <div className={classes.buttons}>
          <Button
            variant="raised"
            color="primary"
            className={classes.submit}
            disabled={level.submitted || level.choice === null}
            onClick={this.handleSubmit}
          >
            Submit</Button>
          <Button
            variant="raised"
            color="default"
            className={classes.next}
            disabled={!level.submitted || level.actual === null}
            onClick={this.handleNext}
          >
            {last ? 'Finish' : 'Next'}
          </Button>
        </div>
      </div>
    );
  }
}

export default Game as ComponentClass<{}>;
