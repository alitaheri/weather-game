import { SWITCH_TEMPERATURE_UNIT } from 'constants/actionTypes';

export function switchTemperatureUnit(unit: 'celsius' | 'fahrenheit') {
  return {
    type: SWITCH_TEMPERATURE_UNIT,
    payload: unit,
  };
}

export type SettingAction =
  | ActionOf<typeof switchTemperatureUnit>;
