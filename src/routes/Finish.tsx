import React, { Component, ComponentClass } from 'react';
import { createStyles, css, Styled } from 'utils/style';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { LEVEL_COUNT } from 'constants/config';
import { restartGame } from 'actions';
import { selectScore } from 'selectors';
import { State } from 'store';

const styles = createStyles(theme => css({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
    margin: 'auto',
  },
  button: {
    width: 300,
  }
}));

function mapStateToProps(state: State) {
  return {
    score: selectScore(state),
  }
}

const actions = { restartGame };

type Props = Styled<typeof styles> & Actions<typeof actions> & ReturnType<typeof mapStateToProps>;

@styles
@connect(mapStateToProps, actions)
class Finish extends Component<Props> {
  private handleRestartButtonClick = () => {
    const { restartGame } = this.props;

    restartGame();
  }

  public render() {
    const { classes, score } = this.props;
    return (
      <div className={classes.root}>
        <Typography variant="display2" gutterBottom={true} align="center">
          You Scored {score} Out of {LEVEL_COUNT}
        </Typography>
        <Button
          classes={{ root: classes.button }}
          variant="raised"
          color="primary"
          onClick={this.handleRestartButtonClick}
        >
          Restart The Game
        </Button>
      </div>
    );
  }
}

export default Finish as ComponentClass<{}>;
