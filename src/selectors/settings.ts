import { State } from 'store';

export function selectTemperatureUnit(state: State) {
  return state.settings.units;
}
