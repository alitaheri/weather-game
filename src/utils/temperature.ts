import { Level } from './levels';

export function getTemperatureString(metric: number, unit: 'celsius' | 'fahrenheit') {
  if (unit === 'celsius') {
    return `${Math.round(metric)}°C`
  }

  return `${Math.round(metric * 1.8 + 32)}°F`;
}

export function getWarmth(level: Level, target: 1 | 2) {
  if (level.city1Temp === null || level.city2Temp === null || level.city1Temp === level.city2Temp) {
    return 'none';
  }

  if (target === 1) {
    return level.city1Temp > level.city2Temp ? 'warm' : 'cold';
  }

  return level.city1Temp > level.city2Temp ? 'cold' : 'warm';
}
