import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { connectRouter, routerMiddleware as createRouterMiddleware } from 'connected-react-router';
import rootReducer from 'reducers';
import rootSaga from 'sagas';

export type State = ReturnType<typeof rootReducer>;

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
const routerMiddleware = createRouterMiddleware(history);

const middlewares = [routerMiddleware, sagaMiddleware];

if (process.env.NODE_ENV !== 'production') {
  const { createLogger } = require('redux-logger');

  middlewares.push(createLogger({ diff: true }));
}

const store = createStore(connectRouter(history)(rootReducer), applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export default store;
