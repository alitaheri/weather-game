import { Action } from 'actions';
import { SWITCH_TEMPERATURE_UNIT } from 'constants/actionTypes';

export interface Settings {
  units: 'celsius' | 'fahrenheit';
}

export default function settingsReducer(state: Settings, action: Action): Settings {
  if (state === undefined) {
    return { units: 'celsius' };
  }

  if (action.type === SWITCH_TEMPERATURE_UNIT) {
    return { ...state, units: action.payload };
  }

  return state;
}
