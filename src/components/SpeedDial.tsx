import React, { Component, ComponentClass } from 'react';
import { createStyles, css, Styled } from 'utils/style';
import MuiSpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SettingsApplications from '@material-ui/icons/SettingsApplications';
import Refresh from '@material-ui/icons/Refresh';
import History from '@material-ui/icons/History';

const styles = createStyles(theme => css({
  root: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3,
  },
}));

export interface SpeedDialProps {
  onSettingsClick(): void;
  onHistoryClick(): void;
  onRestartClick(): void;
  hideHistory: boolean;
  hideRestart: boolean;
}

type Props = SpeedDialProps & Styled<typeof styles>;

interface State {
  open: boolean;
}

@styles
class SpeedDial extends Component<Props, State> {
  public state: State = { open: false };

  private handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  private handleOpen = () => {
    this.setState({ open: true });
  };

  private handleClose = () => {
    this.setState({ open: false });
  };

  private handleSettingsClick = () => {
    this.handleClose();
    this.props.onSettingsClick();
  };

  private handleHistoryClick = () => {
    this.handleClose();
    this.props.onHistoryClick();
  };

  private handleRestartClick = () => {
    this.handleClose();
    this.props.onRestartClick();
  };

  public render() {
    const { classes, hideHistory, hideRestart } = this.props;
    const { open } = this.state;

    return (
      <MuiSpeedDial
        ariaLabel="Settings and History"
        className={classes.root}
        icon={<SpeedDialIcon />}
        onBlur={this.handleClose}
        onClick={this.handleToggle}
        onClose={this.handleClose}
        onFocus={this.handleOpen}
        onMouseEnter={this.handleOpen}
        onMouseLeave={this.handleClose}
        open={open}
      >
        <SpeedDialAction
          icon={<SettingsApplications />}
          tooltipTitle="Settings"
          onClick={this.handleSettingsClick} />
        {!hideHistory && (
          <SpeedDialAction
            icon={<History />}
            tooltipTitle="History"
            onClick={this.handleHistoryClick} />
        )}
        {!hideRestart && (
          <SpeedDialAction
            icon={<Refresh />}
            tooltipTitle="Restart"
            onClick={this.handleRestartClick} />
        )}
      </MuiSpeedDial>
    );
  }
}

export default SpeedDial as ComponentClass<SpeedDialProps>;
