import { takeLatest, call, put, select } from 'redux-saga/effects';
import { replace } from 'connected-react-router';
import { START_GAME, RESTART_GAME } from 'constants/actionTypes';
import { LEVEL_COUNT } from 'constants/config';
import { capitalsLoaded, showError } from 'actions';
import { selectCapitals } from 'selectors';
import { getAllCapitals, Capital } from 'api';
import { buildLevels } from 'utils/levels';

function* loadCapitalsWorker() {
  try {
    const capitals: Capital[] = yield call(getAllCapitals);

    const levels = buildLevels(capitals, LEVEL_COUNT);

    yield put(capitalsLoaded(capitals, levels));

    yield put(replace('/level/1'));
  } catch {
    yield put(showError('Failed to Load Capital Cities'));
  }
}

function* restartWorker() {
  // Use local cache
  const capitals: Capital[] = yield select(selectCapitals);

  const levels = buildLevels(capitals, LEVEL_COUNT);

  yield put(capitalsLoaded(capitals, levels));

  yield put(replace('/level/1'));
}

export default function* loadCapitalsWatcher() {
  yield takeLatest(START_GAME, loadCapitalsWorker);
  yield takeLatest(RESTART_GAME, restartWorker);
}
