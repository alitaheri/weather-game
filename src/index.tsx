// Leaflet Fixes
import 'leaflet/dist/leaflet.css';
import * as L from 'leaflet';

delete (L.Icon.Default.prototype as any)._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import teal from '@material-ui/core/colors/teal';
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from 'store';
import App from 'App';

const theme = createMuiTheme({
  direction: 'rtl',
  typography: { fontFamily: 'roboto' },
  palette: {
    primary: indigo,
    secondary: teal,
  },
});

ReactDOM.render(
  <Fragment>
    <CssBaseline />
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </MuiThemeProvider>
    </Provider>
  </Fragment>,
  document.getElementById('root'),
);
