import React, { Component, ComponentClass, createRef } from 'react';
import * as L from 'leaflet';
import cx from 'classnames';
import { createStyles, css, Styled } from 'utils/style';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import SvgIcon from '@material-ui/core/SvgIcon';
import Check from '@material-ui/icons/Check';
import Close from '@material-ui/icons/Close';
import { Capital } from 'api';

const styles = createStyles(theme => css({
  root: {
    position: 'relative',
    padding: 0,
  },
  mapContainer: {
    width: 200,
    height: 200,
    zIndex: 0,
  },
  mapOverlay: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    textTransform: 'none',
    color: 'white',
    top: 0,
    left: 0,
    width: 200,
    height: 200,
  },
  icon: {
    position: 'absolute',
    top: 100 - 24,
    left: 100 - 24,
    width: 48,
    height: 48,
  },
  temperature: {
    position: 'absolute',
    bottom: theme.spacing.unit,
    width: '100%',
    fontWeight: 'bold',
  },
  warm: {
    color: '#ffac33',
  },
  cold: {
    color: '#00e5ff',
  },
  accepted: {
    color: '#00e676',
  },
  failed: {
    color: '#ff3d00',
  },
}));

export interface CityButtonProps {
  city: Capital;
  selected: boolean;
  onClick(): void;
  disabled?: boolean;
  className?: string;
  warmth: 'cold' | 'warm' | 'none';
  temperature: string | null;
  accepted: boolean;
  failed: boolean;
}

type Props = CityButtonProps & Styled<typeof styles>;

@styles
class CityButton extends Component<Props> {
  private mapContainerRef = createRef<HTMLDivElement>();

  public componentDidMount() {
    const { city: { latlng } } = this.props;
    if (this.mapContainerRef.current) {
      const map = L.map(this.mapContainerRef.current, {
        attributionControl: false,
        zoomControl: false,
        boxZoom: false,
        dragging: false,
        doubleClickZoom: false,
        scrollWheelZoom: false,
      });

      map.setView(latlng, 8, { animate: false });

      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiYWxpdGFoZXJpIiwiYSI6ImNqa2lqeXR4MjEyanYzcXFoajN2d25nbmMifQ.Mr4JS55Kdg_9dCNWDVd7Xw'
      }).addTo(map);

      L.marker(latlng).addTo(map);
    }
  }

  public render() {
    const {
      classes,
      className,
      selected,
      onClick,
      disabled,
      temperature,
      warmth,
      accepted,
      failed,
      city: { name, country },
    } = this.props;

    return (
      <Button disabled={disabled} onClick={onClick} className={cx(className, classes.root)}>
        <div ref={this.mapContainerRef} className={classes.mapContainer} />
        <div className={classes.mapOverlay}>
          <Typography variant="headline" color="inherit">
            {name}
          </Typography>
          <Typography variant="caption" color="inherit">
            {country}
          </Typography>
          {temperature && (
            <Typography
              variant="body1"
              className={cx(classes.temperature, {
                [classes.warm]: warmth === 'warm',
                [classes.cold]: warmth === 'cold',
              })}
            >
              {temperature}
            </Typography>
          )}
          {selected && (
            <SvgIcon
              className={cx(classes.icon, {
                [classes.accepted]: accepted,
                [classes.failed]: failed,
              })}
            >
              {failed ? <Close /> : <Check />}
            </SvgIcon>
          )}
        </div>
      </Button>
    );
  }
}

export default CityButton as ComponentClass<CityButtonProps>;
